<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Bill;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Sale;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Mail;
use App\Utilities\VNPay;

class CheckOutController extends Controller
{
    public function index()
    {
        $carts = Cart::content();
        $total = Cart::total();
        $subtotal = Cart::subtotal();

        return view('frontend.checkout.index', compact('carts', 'total', 'subtotal'));
    }

    public function addOder(Request $request)
    {

        // 1. Them don hang
        $order = Order::create($request->all());

        // 2. Them chi tiet don hang
        $carts = Cart::content();

        foreach ($carts as $cart) {
            $data = [
                "order_id" => $order->id,
                "product_id" => $cart->id,
                "qty" => $cart->qty,
                "amount" => $cart->price,
                "total" => $cart->price * $cart->qty,
            ];

            OrderDetail::create($data);
        }

        if ($request->payment_type == 'pay_later') {


            // 3. Gui email
            $total = Cart::total();
            $subtotal = Cart::subtotal();
            $this->sendEmail($order, $total, $subtotal);

            // 5. Xoa gio hang
            Cart::destroy();

            // 4. Tra ve ket qua
            $stt = 1;
            return view("frontend.checkout.paydone", compact('order', 'total', 'subtotal', "stt"));
        }
        if ($request->payment_type == 'online_payment') {

            // 1. Lấy URL thanh toán VNPay
            $data_url = VNPay::vnpay_create_payment([
                'vnp_TxnRef' => $order->id, // ID của đơn hàng
                'vnp_OrderInfo' => "Mô tả đơn hàng",
                'vnp_Amount' => $cart->price,
            ]);
            // 2. Chuyển hướng tới URL lấy được
            return redirect()->to($data_url);
        }
    }

    public function vnPayCheck(Request $request)
    {

        // 1. Lấy data từ URL (do VNPay gửi về qua $vnp_Returnurl)
        $vnp_ResponseCode = $request->get('vnp_ResponseCode'); // Mã phản hồi kết quả thanh toán. 00 = thành công
        $vnp_TxnRef = $request->get('vnp_TxnRef'); // ticket_id
        $vnp_Amount = $request->get('vnp_Amount'); // Số tiền thanh toán.
        // 2. Kiểm tra kết quả giao dịch trả về từ VNPay
        if ($vnp_ResponseCode != null) {
            // Nếu thành công
            if ($vnp_ResponseCode == 00) {
                // Gửi email
                $order = Order::find($vnp_TxnRef);
                $total = Cart::total();
                $subtotal = Cart::subtotal();
                $this->sendEmail($order, $total, $subtotal);

                // Xóa giỏ hàng
                Cart::destroy($order);

                // Thông báo kết quả.
                $stt = 1;
                return view("frontend.checkout.paydone", compact('order', 'total', 'subtotal', "stt"));
            } else {
                // Nếu không thành công
                // Xóa đơn hàng đã thêm vào Database
                Order::find($vnp_TxnRef)->delete();

                //trả về thông báo lỗi
                return 'ERROR: Payment failed or canceled';
            }
        }
    }

    private function sendEmail($order, $total, $subtotal = 0)
    {
        $email_to = $order->email;

        Mail::send('frontend.checkout.email', compact('order', 'total', 'subtotal'), function ($message) use ($email_to) {
            $message->from('nhandevweb@gmail.com', 'Shop sport');
            $message->to($email_to, $email_to);
            $message->subject('Order Notification');
        });
    }
}

// Thông tin thẻ test
// #	Thông tin thẻ	Ghi chú
// 1
// Ngân hàng: NCB
// Số thẻ: 9704198526191432198
// Tên chủ thẻ:NGUYEN VAN A
// Ngày phát hành:07/15
// Mật khẩu OTP:123456
// Thành công
// 2
// Ngân hàng: NCB
// Số thẻ: 9704195798459170488
// Tên chủ thẻ:NGUYEN VAN A
// Ngày phát hành:07/15
// Thẻ không đủ số dư
// 3
// Ngân hàng: NCB
// Số thẻ: 9704192181368742
// Tên chủ thẻ:NGUYEN VAN A
// Ngày phát hành:07/15
// Thẻ chưa kích hoạt
// 4
// Ngân hàng: NCB
// Số thẻ: 9704193370791314
// Tên chủ thẻ:NGUYEN VAN A
// Ngày phát hành:07/15
// Thẻ bị khóa
// 5
// Ngân hàng: NCB
// Số thẻ: 9704194841945513
// Tên chủ thẻ:NGUYEN VAN A
// Ngày phát hành:07/15
// Thẻ bị hết hạn
