<?php

namespace App\Utilities;

class VNPay
{
    // use HasFactory;
    // static $vnp_TmnCode = "C3HCR14Y";
    // static $vnp_HashSecret = "CLOLOWUHYXUYFVLOGILDYSPIQVMKXXWQ";
    // static $vnp_Url = "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
    // static $vnp_Returnurl = "/checkout/vnPayCheck";


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    static $vnp_TmnCode = "765ZD8RR"; //Website ID in VNPAY System
    static $vnp_HashSecret = "JILDMIGMMQRFSQNYLUKTTXZTCHNDPPLK"; //Secret key
    static $vnp_Url = "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
    static $vnp_Returnurl = "/checkout/vnPayCheck";
    static $vnp_apiUrl = "http://sandbox.vnpayment.vn/merchant_webapi/merchant.html";
    //Config input format
    //Expire

    public static function vnpay_create_payment(array $data)
    {
        error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
        $vnp_TxnRef = $data["vnp_TxnRef"];
        $vnp_OrderInfo = $data["vnp_OrderInfo"];
        $vnp_OrderType = "billpayment";
        $vnp_Amount = $data["vnp_Amount"] * 100;
        $vnp_Locale = "vn";
        $vnp_IpAddr = $_SERVER["REMOTE_ADDR"];

        $inputData = array(
            "vnp_Version" => "2.1.0",
            "vnp_TmnCode" => self::$vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $vnp_OrderInfo,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => 'http://127.0.1:8000/checkout/vnPayCheck',
            "vnp_TxnRef" => $vnp_TxnRef,
        );

        // Thêm bankCode
        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }

        ksort($inputData);
        $query = "";
        $i = 0;
        $hashdata = "";
        // foreach($inputData as $key => $value) {
        //     if($i == 1) {
        //         $hashdata .= '&' .$key . "=" . $value;
        //     }else {
        //         $hashdata .= $key . "=" . $value;
        //         $i =1;
        //     }
        //     $query .= urlencode($key) . "=" . urlencode($value) . '&';
        // }

        // (string)$vnp_Url = self::$vnp_Url . "?" . $query;
        // if(isset(self::$vnp_HashSecret)) {
        //     $vnpSecureHash = hash('sha256', self::$vnp_HashSecret . $hashdata);
        //     (string)$vnp_Url .= 'vnp_SecureHashType=SHA256&vnp_SecureHash=' . $vnpSecureHash;
        //     dd($vnpSecureHash, $vnp_Url);
        // }

        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashdata .= '&' . urlencode($key) . "=" . urlencode($value);
            } else {
                $hashdata .= urlencode($key) . "=" . urlencode($value);
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = self::$vnp_Url . "?" . $query;
        if (isset(self::$vnp_HashSecret)) {
            $vnpSecureHash =   hash_hmac('sha512', $hashdata, self::$vnp_HashSecret);
            $vnp_Url .= 'vnp_SecureHash=' . $vnpSecureHash;
        }

        $returnData = array('code' => '00',
            'message' => 'success',
            'data' => (string)$vnp_Url);
        return $returnData['data'];

    }
}
